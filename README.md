## Bayshann Coding Test

### The problem

Staff wish to gain some insights into the current student enrollment. They need an application that will help them understand which students are enrolled in which classes.

### Materials

You are provided with some student timetable data in this challenge.

Feel free to utilise external packages or libraries that might make your life easier for either task.

### Task 1 - REST API

Provide a REST API using Node.js that will ingest the provided data, and outputs the following:

1) a list of unique courses and their associated IDs

2) a list of unique student IDs given a particular course ID.

It's up to you how you accomplish this.

#### Data structure:

``` json
{
    "student_id": "18362887",
    "class_details": [
      {
        "subject_code": "ACC3TAX",
        "subject_desc": "Taxation",
        "week_start_date": "2017-11-13",
        "week_end_date": "2017-11-19",
        "exact_class_date": "2017-11-13",
        "day_of_week": "Mon",
        "room_number": "1.09",
        "room": "1.09",
        "gps_coordinates": "-33.875641,151.209343",
        "start_time": "09:00",
        "end_time": "12:00",
        "campus_code": "SY",
        "hasStandardRoomDescription": false,
        "duration": 180,
        "duration_code": "I",
        "isHoliday": false
      }
    ]
  }

```

### Task 2 - Web UI

Create a Single-page app with Angular (4+) or React.js that delivers the following:

1) display an initial list of the unique courses

2) once a course is selected, display a sub-list of all the unique student IDs that participate in that course

3) utilises the REST API endpoint you developed in Step 1.

### Pointers

Estimated time to complete this task is 1-2 hours.

### What we look at

- Coding Style
- Semantics
- Problem Solving
- Algorithms
- Design Patterns
- Error Handling
- User experience
